# Game-Action-Method-on-Java
import javax.microedition.lcdui.*;

public class KeyCanvas
	extends Canvas {
     private Font mFont;
     private String mMessage = "[Press keys]";
	
     public KeyCanvas() {
	     mFont = Font.getFont(Font.FACE_PROPORTONAL,
	             Font.STYLE_PLAIN, Font.SIZE_MEDIUM);
     }
     
     public void paint(Graphics g){
	     int w = getWidth();
	     int h = getHeight();
	     
	     // Clear the Canvas.
	     g.setGrayScale(255);
	     gfillRect(0, 0, w - 1, h -1);
	     
	     g.setFont(mFont);
	     
	     int x = w / 2 ;
	     int y = h / 2;
	     
	     g.drawString(mMessage, x , y, Graphics.BASELINE | Graphics.HCENTER);
    }
	  
	  protected void keyPressed(int keyCode) {
		  int gameAction = getGameAction(keyCode);
		  switch(gameAction) {
			  case UP:       mMessage = "UP";                                 break;
			  case Down:  mMessage = "Down";                             break;
			  case    Left:  mMessage = "Left";                                break;
			  case  Right:  mMessage = "Right";                             break;
			  case  FIRE:   mMessage =  "FIRE";                              break;
			  case  GAME_A mMessage = "GAME_A";                       break;
			  case  Game_B mMessage = "Game_B";                       break;
			  default:          mMessage = "";break;
		        }
			repaint();
		 }
	 }
